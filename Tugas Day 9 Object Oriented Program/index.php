<?php

require_once ('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal('shaun');

echo "Nama : " .$sheep ->name. "<br>";
echo "Jumlah kaki : " .$sheep ->legs. "<br>";
echo "cold blooded : " .$sheep ->cold_blooded. "<br> <br>";

$kodok = new frog('buduk');
echo "Nama : " .$kodok ->name. "<br>";
echo "Jumlah kaki : " .$kodok ->legs. "<br>";
echo "cold blooded : " .$kodok ->cold_blooded. "<br>";
echo "Jump : " .$kodok -> jump(). "<br> <br>";

$sungokong = new ape('kera sakti');
echo "Nama : " .$sungokong ->name. "<br>";
echo "Jumlah kaki : " .$sungokong ->legs. "<br>";
echo "cold blooded : " .$sungokong ->cold_blooded. "<br>";
echo "Yell : " .$sungokong -> yell();

?>