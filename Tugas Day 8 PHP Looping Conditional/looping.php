<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Contoh Looping</h1>
    <?php 

    echo "<h2> Soal 1</h2>";
    echo"<h4>Looping 1</h4>";

    for ($i=2;$i<=20;$i+=2){
        echo $i . " - I Love PHP<br>";
    }

    echo "<h4>Looping</h4>";

    for ($a=20;$a>=2;$a-=2){
        echo $a . "- I Love PHP<br>";
    }

    echo "<h2>Soal 2</h2>";
    $angka = [18, 45, 29, 61, 47, 34];
    
    echo "array numbers:";
    print_r($angka);
    // Lakukan Looping di sini
    echo "<br>";
    echo "Array sisa baginya adalah: ";

    foreach($angka as $value){
        $rest[]=$value%5;
    }
    print_r($rest);
    echo"<br>";

    echo "<h2>Soal 3 </h2>";
    $items =[
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];

    foreach($items as $key =>$value){
        $tampung = array(
           'id'=> $value[0],
           'name'=>$value[1],
           'price'=>$value[2],
           'description'=>$value[3],
           'source'=>$value[4]
        );
        print_r($tampung);
        echo "<br>";
    }

    echo "<h2>Soal 4 Asterix</h2>";
    for($j=1;$j <=5 ; $j++){
        for($b=1 ;$b<= $j; $b++){
            echo"*";
        }
        echo "<br>";
    }
    ?>

</body>
</html>